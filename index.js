const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const fs = require('fs');

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true)
    }
    return (false)
}

const trueEmailUsers = [];
const falseEmailUsers = [];

fs.createReadStream('sample.csv')
    .pipe(csv())
    .on('data', (row) => {
        // console.log(row);
        if (row.email ) {
            if (ValidateEmail(row.email)) {
                trueEmailUsers.push(row);  // move to output csv
            } else {
                falseEmailUsers.push(row); // move to error csv
            }
        } else {
            falseEmailUsers.push(row); // move to error csv 
        }

    })
    .on('end', () => {
        console.log('CSV file successfully processed');
        downloadFiles({fileName : 'true_email.csv', data : trueEmailUsers });
        downloadFiles({fileName : 'false_email.csv', data : falseEmailUsers });
    });


function downloadFiles({fileName, data}) {
    if(!data) { data = []; }

    const csvWriter = createCsvWriter({
        path: fileName,
        header: [
            { id: 'email', title: 'Email' },
            // { id: 'surname', title: 'Surname' },
            // { id: 'age', title: 'Age' },
            // { id: 'gender', title: 'Gender' },
        ]
    });

    csvWriter
        .writeRecords(data)
        .then(() => console.log(`The ${fileName} CSV file was written successfully`));

}




































    // downloadCSV = () => {
    //     const msg = "Are you sure you want to download the CSV of current list? ";
    //     if (confirm(msg)) {
    //        let items = this.state.items;
    //        let keys = ['email', 'phone', 'score', 'college', 'first_name', 'last_name', 'role', 'status', "college_year",'college_stream', custom_score, 'score_90', 'updatedAt', 'createdAt'];
    //        let headers = keys.join(',');
    //        // console.log(headers)
    //        let str = '';
    //        for (let i = 0; i < items.length; i++) {
    //           let obj = items[i];
    //           for (let j = 0; j < keys.length; j++) {
    //              const key = keys[j];
    //              if (obj.hasOwnProperty(key)) {
    //                 if (key === 'role') {
    //                    str += `${obj[key]['title']},`
    //                 } else {
    //                    if (typeof obj[key] !== 'undefined') {
    //                       str += `${obj[key]},`
    //                    } else {
    //                       str += `,`
    //                    }
    //                 }
    //              } else {
    //                 str += ',';
    //              }
    //           }
    //           str += '\n'
    //        }
    //        const csv = `${headers} \n ${str}`;
    //        console.log("file downloading");
    //        var hiddenElement = document.createElement('a');
    //        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    //        hiddenElement.target = '_blank';
    //        hiddenElement.download = 'userdata.csv';
    //        hiddenElement.click();
    //     }
    //  }